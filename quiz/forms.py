from django import forms
from django.forms.widgets import RadioSelect, Textarea
import registration

class QuestionForm(forms.Form):
    def __init__(self, question, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        choice_list = [x for x in question.get_answers_list()]
        self.fields["answers"] = forms.ChoiceField(choices=choice_list,
                                                   widget=RadioSelect)


class EssayForm(forms.Form):
    def __init__(self, question, *args, **kwargs):
        super(EssayForm, self).__init__(*args, **kwargs)
        self.fields["answers"] = forms.CharField(
            widget=Textarea(attrs={'style': 'width:100%'}))

from registration.users import UserModel
from registration.users import UsernameField

User = UserModel()

from registration.forms import RegistrationForm


class CustomForm(RegistrationForm):

    required_css_class = 'required'
    email = None
    # email = forms.EmailField(label="E-mail", widget=forms.EmailInput(attrs={'placeholder': 'Email address', 'class': 'form-control'}))
    password1 = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'placeholder': 'password', 'class': 'form-control'}))
    password2 = forms.CharField(label="Password confirmation",
                                widget=forms.PasswordInput(attrs={'placeholder': 'confirm password', 'class': 'form-control'}),
                                help_text="Enter the same password as above, for verification.")

    class Meta:
        model = User
        fields = (UsernameField(), )
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': 'username', 'class': 'form-control'}),
        }
